# Docker image with native-executable

We can compile the Java code into a native executable that can be run in
a Docker container:

```bash
./gradlew clean buildNative --stacktrace --info --docker-build=true -Dquarkus.native.builder-image=quay.io/quarkus/ubi-quarkus-native-image:19.3.1-java8 docker dockerTag 
``` 

This will create the native executable using the specified Quarkus
native-image builder. Once done, it will create a Docker image and
tag it three ways:

```
fluxproxy:<VERSION>
registry.gitlab.com/hazsetata/fluxproxy-native:<VERSION>
registry.gitlab.com/hazsetata/fluxproxy-native:latest 
```

If you want to push all the tags to the defined registry (and you
have the right to write that registry), you can execute:

```bash
./gradlew dockerTagsPush
```

Note: This process will produce a 64bit Linux executable, so if working
on macOS, it can't be executed on the machine any longer.
