package com.hazse.fluxproxy;

import com.hazse.fluxproxy.event.AbstractEvent;
import com.hazse.fluxproxy.event.SyncEvent;
import com.hazse.fluxproxy.event.attribute.EventType;
import com.hazse.fluxproxy.event.attribute.ResourceId;
import com.hazse.fluxproxy.serialization.EventDeserializationHelper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class EventParsingTest {
    @Inject
    EventDeserializationHelper eventDeserializationHelper;

    @Test
    public void parseSyncEvent() throws IOException {
        try (InputStream stream = getClass().getResourceAsStream("/sync_event.json")) {
            AbstractEvent event = eventDeserializationHelper.deserializeEventFrom(stream);

            assertNotNull(event);
            assertNotNull(event.getStartedAt());
            assertNotNull(event.getEndedAt());
            assertEquals(0, event.getId());
            assertEquals(EventType.Sync, event.getType());

            assertNotNull(event.getAffectedResources());
            assertEquals(16, event.getAffectedResources().size());

            int clusterResourceCount = 0;
            for (ResourceId resourceId : event.getAffectedResources()) {
                if (resourceId.getNamespace() == null) {
                    clusterResourceCount++;
                }
            }
            assertEquals(1, clusterResourceCount);

            assertTrue(event instanceof SyncEvent);
            SyncEvent syncEvent = (SyncEvent)event;

            assertNotNull(syncEvent.getMetadata());
            assertTrue(syncEvent.getMetadata().isInitialSync());
            assertNotNull(syncEvent.getMetadata().getCommits());
            assertEquals(6, syncEvent.getMetadata().getCommits().size());
        }
    }
}
