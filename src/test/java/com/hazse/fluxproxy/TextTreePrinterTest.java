package com.hazse.fluxproxy;

import com.hazse.fluxproxy.texttree.StringBufferPrinter;
import com.hazse.fluxproxy.texttree.TextTreeNode;
import com.hazse.fluxproxy.texttree.TextTreePrinter;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class TextTreePrinterTest {
    @Test
    public void testTreePrinter_simple() {
        StringBuffer testTarget = new StringBuffer();
        StringBufferPrinter bufferPrinter = new StringBufferPrinter(testTarget, "\n");
        TextTreePrinter treePrinter = new TextTreePrinter();

        TextTreeNode root = new TextTreeNode("root");
        TextTreeNode a = root.addChild("a");
        a.addChild("a-1");
        TextTreeNode a2 = a.addChild("a-2");
        a.addChild("a-3");
        TextTreeNode b = a2.addChild("b");
        b.addChild("b-1");
        b.addChild("b-2");
        TextTreeNode c = root.addChild("c");
        TextTreeNode d = c.addChild("d");
        d.addChild("e");
        root.addChild("f");

        treePrinter.printTree(root, bufferPrinter);

        assertEquals(
                "root\n" +
                "├── a\n" +
                "│   ├── a-1\n" +
                "│   ├── a-2\n" +
                "│   │   └── b\n" +
                "│   │       ├── b-1\n" +
                "│   │       └── b-2\n" +
                "│   └── a-3\n" +
                "├── c\n" +
                "│   └── d\n" +
                "│       └── e\n" +
                "└── f\n", testTarget.toString());
    }

    @Test
    public void testTreePrinter_multi() {
        StringBuffer testTarget = new StringBuffer();
        StringBufferPrinter bufferPrinter = new StringBufferPrinter(testTarget, "\n");
        TextTreePrinter treePrinter = new TextTreePrinter();

        TextTreeNode root = new TextTreeNode("root", "Lorem ipsum", "dolor sit amet");
        TextTreeNode a = root.addChild("a");
        a.addChild("a-1");
        TextTreeNode a2 = a.addChild("a-2");
        a.addChild("a-3");
        TextTreeNode b = a2.addChild("b", "Consectetur adipiscing elit.", "Vivamus quis.");
        b.addChild("b-1");
        b.addChild("b-2");
        TextTreeNode c = root.addChild("c");
        TextTreeNode d = c.addChild("d");
        d.addChild("e");
        root.addChild("f");

        treePrinter.printTree(root, bufferPrinter);

        assertEquals(
                "root\n" +
                        "Lorem ipsum\n" +
                        "dolor sit amet\n" +
                        "├── a\n" +
                        "│   ├── a-1\n" +
                        "│   ├── a-2\n" +
                        "│   │   └── b\n" +
                        "│   │       Consectetur adipiscing elit.\n" +
                        "│   │       Vivamus quis.\n" +
                        "│   │       ├── b-1\n" +
                        "│   │       └── b-2\n" +
                        "│   └── a-3\n" +
                        "├── c\n" +
                        "│   └── d\n" +
                        "│       └── e\n" +
                        "└── f\n", testTarget.toString());
    }
}
