package com.hazse.fluxproxy.handlers;

import com.hazse.fluxproxy.event.AbstractEvent;
import com.hazse.fluxproxy.serialization.EventDeserializationHelper;
import com.hazse.fluxproxy.texttree.StringBufferPrinter;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
class LogEventHandlerTest {
    @Inject
    EventDeserializationHelper eventDeserializationHelper;

    @Test
    public void parseSyncEvent() throws IOException {
        try (InputStream stream = getClass().getResourceAsStream("/sync_event.json")) {
            AbstractEvent event = eventDeserializationHelper.deserializeEventFrom(stream);
            assertNotNull(event);

            StringBuffer testTarget = new StringBuffer();
            StringBufferPrinter bufferPrinter = new StringBufferPrinter(testTarget, "\n");
            LogEventHandler eventHandler = new LogEventHandler(bufferPrinter);

            eventHandler.handleFluxEvent(event);

            assertEquals(
                    "Sync event (id: 0)\n" +
                    "Started at : 2019-11-25T19:44:06.444575175Z[UTC]\n" +
                    "Finished at: 2019-11-25T19:44:06.444575175Z[UTC]\n" +
                    "├── Affected resources\n" +
                    "│   ├── namespace/vtt\n" +
                    "│   ├── [vtt] deployment/art\n" +
                    "│   ├── [vtt] deployment/bands\n" +
                    "│   ├── [vtt] deployment/links\n" +
                    "│   ├── [vtt] deployment/management\n" +
                    "│   ├── [vtt] deployment/webclient\n" +
                    "│   ├── [vtt] mapping/art\n" +
                    "│   ├── [vtt] mapping/bands\n" +
                    "│   ├── [vtt] mapping/links\n" +
                    "│   ├── [vtt] mapping/management\n" +
                    "│   ├── [vtt] mapping/webclient\n" +
                    "│   ├── [vtt] service/art\n" +
                    "│   ├── [vtt] service/bands\n" +
                    "│   ├── [vtt] service/links\n" +
                    "│   ├── [vtt] service/management\n" +
                    "│   └── [vtt] service/webclient\n" +
                    "├── Commits\n" +
                    "│   ├── More K8s yaml fixes\n" +
                    "│   │   Revision: 7fe3042ef1aa601f91c9940db7ed0539645df823\n" +
                    "│   ├── Fixed typo in webclient mapping.\n" +
                    "│   │   Revision: 0b86d437ecc7e3b58a7961e1208ad6252176cdba\n" +
                    "│   ├── Fixed bad namespace in ambassador mappings.\n" +
                    "│   │   Revision: 3ad2a87e3cde4a75f9bd47422d7067884d4803f0\n" +
                    "│   ├── Added web-client deployment to GitOps\n" +
                    "│   │   Revision: bba6960a52cccf1bbdaefc0b484aaf1452e73900\n" +
                    "│   ├── Added longer initial delay to api servers.\n" +
                    "│   │   Revision: 21457ed4f80a0bc9539d95bb3ae465911e63dbed\n" +
                    "│   └── API services K8s yaml added.\n" +
                    "│       Revision: 9cdbf58709d48091e3eeae4bf9e3c03db7af3794\n" +
                    "└── Initial sync: true\n",
                    testTarget.toString()
            );
        }
    }

}