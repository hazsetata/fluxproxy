package com.hazse.fluxproxy.serialization;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazse.fluxproxy.event.AbstractEvent;
import com.hazse.fluxproxy.event.SyncEvent;
import com.hazse.fluxproxy.event.UnknownEvent;
import com.hazse.fluxproxy.event.attribute.EventType;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.InputStream;

@ApplicationScoped
public class EventDeserializationHelper {
    private static final Logger log = Logger.getLogger("deserializer");

    @Inject
    ObjectMapper objectMapper;

    public AbstractEvent deserializeEventFrom(String eventMessage) {
        JsonNode node;
        try {
            node = objectMapper.readTree(eventMessage);
        }
        catch (Exception e) {
            log.error("Can't parse event data.", e);

            node = null;
        }

        if (node != null) {
            return convertJsonNode(node);
        }

        return null;
    }

    public AbstractEvent deserializeEventFrom(InputStream eventMessage) {
        JsonNode node;
        try {
            node = objectMapper.readTree(eventMessage);
        }
        catch (Exception e) {
            log.error("Can't parse event data.", e);

            node = null;
        }

        if (node != null) {
            return convertJsonNode(node);
        }

        return null;
    }

    private AbstractEvent convertJsonNode(JsonNode node) {
        if (node.has("type")) {
            JsonNode typeNode = node.get("type");

            try {
                EventType eventType = objectMapper.treeToValue(typeNode, EventType.class);

                switch (eventType) {
                    case Sync: return objectMapper.treeToValue(node, SyncEvent.class);
                    default: return objectMapper.treeToValue(node, UnknownEvent.class);
                }
            }
            catch (Exception e) {
                log.error("Error during JSON processing.", e);
            }
        }
        else {
            log.error("No event-type available in the JSON.");
        }

        return null;
    }
}
