package com.hazse.fluxproxy.serialization;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.hazse.fluxproxy.event.attribute.ResourceId;

public class FluxModule extends SimpleModule {
    private static final long serialVersionUID = 1L;

    public FluxModule() {
        addDeserializer(ResourceId.class, new ResourceIdDeserializer());
    }
}
