package com.hazse.fluxproxy.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.hazse.fluxproxy.event.attribute.ResourceId;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResourceIdDeserializer extends StdDeserializer<ResourceId> {
    private static String CLUSTER_RESOURCE_INDICATOR = "<cluster>";
    private static Pattern idPattern = Pattern.compile("^(<cluster>|[a-zA-Z0-9_-]+):([a-zA-Z0-9_-]+)/([a-zA-Z0-9_.:-]+)$");

    public ResourceIdDeserializer() {
        super(ResourceId.class);
    }

    @Override
    public ResourceId deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonToken curr = p.getCurrentToken();

        if (curr == JsonToken.VALUE_STRING || curr == JsonToken.FIELD_NAME) {
            final String value = p.getText();

            Matcher valueMatcher = idPattern.matcher(value);

            if (valueMatcher.matches()) {
                return new ResourceId(
                        valueMatcher.group(1).equals(CLUSTER_RESOURCE_INDICATOR) ? null : valueMatcher.group(1),
                        valueMatcher.group(2),
                        valueMatcher.group(3)
                );
            }
        }

        return null;
    }
}