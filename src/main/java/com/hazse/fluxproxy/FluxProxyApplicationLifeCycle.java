package com.hazse.fluxproxy;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class FluxProxyApplicationLifeCycle {
    private static final Logger log = Logger.getLogger("io.quarkus");

    void onStart(@Observes StartupEvent ev) {
        log.info(" _______  ___      __   __  __   __  _______  ______    _______  __   __  __   __ ");
        log.info("|       ||   |    |  | |  ||  |_|  ||       ||    _ |  |       ||  |_|  ||  | |  |");
        log.info("|    ___||   |    |  | |  ||       ||    _  ||   | ||  |   _   ||       ||  |_|  |");
        log.info("|   |___ |   |    |  |_|  ||       ||   |_| ||   |_||_ |  | |  ||       ||       |");
        log.info("|    ___||   |___ |       | |     | |    ___||    __  ||  |_|  | |     | |_     _|");
        log.info("|   |    |       ||       ||   _   ||   |    |   |  | ||       ||   _   |  |   |  ");
        log.info("|___|    |_______||_______||__| |__||___|    |___|  |_||_______||__| |__|  |___|  ");
        log.info("");
    }

    void onStop(@Observes ShutdownEvent ev) {
        log.info("The application is stopping...");
    }
}
