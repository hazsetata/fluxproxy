package com.hazse.fluxproxy.util;

import com.hazse.fluxproxy.event.attribute.ResourceId;

import java.util.Comparator;

public class NamespaceComparator implements Comparator<ResourceId> {
    @Override
    public int compare(ResourceId o1, ResourceId o2) {
        String n1 = o1.getNamespace();
        String n2 = o2.getNamespace();

        if ((n1 == null) && (n2 == null)) {
            return 0;
        }
        else if ((n1 != null) && (n2 != null)) {
            return n1.compareTo(n2);
        }
        else if (n1 == null) {
            return -1;
        }
        else {
            return 1;
        }
    }
}
