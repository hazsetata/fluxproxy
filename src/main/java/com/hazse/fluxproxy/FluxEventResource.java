package com.hazse.fluxproxy;

import com.hazse.fluxproxy.event.AbstractEvent;
import com.hazse.fluxproxy.event.UnknownEvent;
import com.hazse.fluxproxy.handlers.IFluxEventHandler;
import com.hazse.fluxproxy.handlers.LogEventHandler;
import com.hazse.fluxproxy.serialization.EventDeserializationHelper;
import com.hazse.fluxproxy.texttree.LoggerPrinter;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/api/v1/fluxevents/v6/events")
public class FluxEventResource {
    private static final Logger log = Logger.getLogger("fluxevent");
    private static final Logger eventLog = Logger.getLogger("fluxlogger");
    private static final LoggerPrinter LOGGER_PRINTER = new LoggerPrinter(eventLog);

    @Inject
    EventDeserializationHelper eventDeserializationHelper;

    private IFluxEventHandler eventHandler;

    public FluxEventResource() {
        this.eventHandler = new LogEventHandler(LOGGER_PRINTER);
    }

    @POST
    public Response receiveEvent(String eventMessage) {
        AbstractEvent event = eventDeserializationHelper.deserializeEventFrom(eventMessage);
        if (event instanceof UnknownEvent) {
            log.info(eventMessage);
        }
        eventHandler.handleFluxEvent(event);

        return Response.ok().build();
    }
}
