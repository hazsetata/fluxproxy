package com.hazse.fluxproxy.event.attribute;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class ResourceId {
    private String namespace;
    private String kind;
    private String name;

    public ResourceId() {
    }

    public ResourceId(String namespace, String kind, String name) {
        this.namespace = namespace;
        this.kind = kind;
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
