package com.hazse.fluxproxy.event.attribute;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public enum EventLogLevel {
    @JsonProperty("debug")
    Debug,
    @JsonProperty("info")
    Info,
    @JsonProperty("warn")
    Warning,
    @JsonProperty("error")
    Error
}
