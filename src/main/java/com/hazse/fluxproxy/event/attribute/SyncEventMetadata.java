package com.hazse.fluxproxy.event.attribute;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.List;
import java.util.Map;

@RegisterForReflection
@JsonIgnoreProperties(ignoreUnknown = true)
public class SyncEventMetadata {
    private List<GitCommit> commits;
    @JsonProperty("includes")
    private Map<String, Boolean> includedCommitTypes;
    private List<ResourceError> errors;
    private boolean initialSync;

    public List<GitCommit> getCommits() {
        return commits;
    }

    public void setCommits(List<GitCommit> commits) {
        this.commits = commits;
    }

    public Map<String, Boolean> getIncludedCommitTypes() {
        return includedCommitTypes;
    }

    public void setIncludedCommitTypes(Map<String, Boolean> includedCommitTypes) {
        this.includedCommitTypes = includedCommitTypes;
    }

    public List<ResourceError> getErrors() {
        return errors;
    }

    public void setErrors(List<ResourceError> errors) {
        this.errors = errors;
    }

    public boolean isInitialSync() {
        return initialSync;
    }

    public void setInitialSync(boolean initialSync) {
        this.initialSync = initialSync;
    }
}
