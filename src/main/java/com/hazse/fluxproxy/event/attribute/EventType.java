package com.hazse.fluxproxy.event.attribute;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public enum EventType {
    @JsonProperty("commit")
    Commit,
    @JsonProperty("sync")
    Sync,
    @JsonProperty("release")
    Release,
    @JsonProperty("autorelease")
    AutoRelease("Auto-release"),
    @JsonProperty("automate")
    Automate,
    @JsonProperty("deautomate")
    Deautomate,
    @JsonProperty("lock")
    Lock,
    @JsonProperty("unlock")
    Unlock,
    @JsonProperty("update_policy")
    UpdatePolicy("Update policy"),
    @JsonProperty("other")
    Other;

    private String printedName;

    EventType() {
        this(null);
    }

    EventType(String printedName) {
        this.printedName = printedName;
    }

    public String getPrintedName() {
        if (printedName != null) {
            return printedName;
        }
        else {
            return this.name();
        }
    }
}
