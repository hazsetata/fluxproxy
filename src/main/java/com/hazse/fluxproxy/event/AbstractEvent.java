package com.hazse.fluxproxy.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hazse.fluxproxy.event.attribute.EventLogLevel;
import com.hazse.fluxproxy.event.attribute.EventType;
import com.hazse.fluxproxy.event.attribute.ResourceId;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.time.ZonedDateTime;
import java.util.List;

@RegisterForReflection
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractEvent {
    private long id;
    private EventType type;
    private ZonedDateTime startedAt;
    private ZonedDateTime endedAt;
    private EventLogLevel logLevel;
    private String message;
    @JsonProperty("serviceIDs")
    private List<ResourceId> affectedResources;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(ZonedDateTime endedAt) {
        this.endedAt = endedAt;
    }

    public EventLogLevel getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(EventLogLevel logLevel) {
        this.logLevel = logLevel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResourceId> getAffectedResources() {
        return affectedResources;
    }

    public void setAffectedResources(List<ResourceId> affectedResources) {
        this.affectedResources = affectedResources;
    }
}
