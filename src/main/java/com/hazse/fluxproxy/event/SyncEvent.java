package com.hazse.fluxproxy.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hazse.fluxproxy.event.attribute.SyncEventMetadata;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
@JsonIgnoreProperties(ignoreUnknown = true)
public class SyncEvent extends AbstractEvent{
    private SyncEventMetadata metadata;

    public SyncEventMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SyncEventMetadata metadata) {
        this.metadata = metadata;
    }
}
