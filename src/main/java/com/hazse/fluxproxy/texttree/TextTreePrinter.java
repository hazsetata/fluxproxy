package com.hazse.fluxproxy.texttree;

import java.util.Iterator;
import java.util.List;

/**
 * Basic idea from: https://stackoverflow.com/questions/4965335/how-to-print-binary-tree-diagram
 */
public class TextTreePrinter {
    public void printTree(TextTreeNode rootNode, IPrinter printer) {
        print(printer, rootNode, "", "");
    }

    private void print(IPrinter printer, TextTreeNode node, String prefix, String childrenPrefix) {
        printContent(printer, node.getContent(), prefix, childrenPrefix);
        if (node.hasChildren()) {
            for (Iterator<TextTreeNode> it = node.getChildren().iterator(); it.hasNext();) {
                TextTreeNode childNode = it.next();

                if (it.hasNext()) {
                    print(printer, childNode, childrenPrefix + "├── ", childrenPrefix + "│   ");
                }
                else {
                    print(printer, childNode, childrenPrefix + "└── ", childrenPrefix + "    ");
                }
            }
        }
    }

    private void printContent(IPrinter printer, List<String> nodeContent, String prefix, String childrenPrefix) {
        boolean first = true;

        for (String contentLine : nodeContent) {
            if (first) {
                printer.print(prefix + contentLine);
            }
            else {
                printer.print(childrenPrefix + contentLine);
            }

            first = false;
        }
    }
}
