package com.hazse.fluxproxy.texttree;

import org.jboss.logging.Logger;

public class LoggerPrinter implements IPrinter{
    private Logger log;
    private Logger.Level level;

    public LoggerPrinter(Logger log) {
        this(log, Logger.Level.INFO);
    }

    public LoggerPrinter(Logger log, Logger.Level level) {
        this.log = log;
        this.level = level;
    }

    @Override
    public void print(String text) {
        log.log(level, text);
    }
}
