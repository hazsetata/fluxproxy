package com.hazse.fluxproxy.texttree;

public interface IPrinter {
    void print(String text);
}
