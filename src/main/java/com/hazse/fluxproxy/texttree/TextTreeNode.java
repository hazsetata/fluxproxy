package com.hazse.fluxproxy.texttree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TextTreeNode {
    private List<String> content;
    private List<TextTreeNode> children;

    public TextTreeNode(String content) {
        this(Collections.singletonList(content));
    }

    public TextTreeNode(String... content) {
        this(Arrays.asList(content));
    }

    public TextTreeNode(List<String> content) {
        this.content = content;
        this.children = null;
    }

    public TextTreeNode addChild(String content) {
        if (children == null) {
            children = new ArrayList<>();
        }

        TextTreeNode retValue = new TextTreeNode(content);
        children.add(retValue);

        return retValue;
    }

    public TextTreeNode addChild(String... content) {
        if (children == null) {
            children = new ArrayList<>();
        }

        TextTreeNode retValue = new TextTreeNode(content);
        children.add(retValue);

        return retValue;
    }

    public TextTreeNode addChild(List<String> content) {
        if (children == null) {
            children = new ArrayList<>();
        }

        TextTreeNode retValue = new TextTreeNode(content);
        children.add(retValue);

        return retValue;
    }

    public List<String> getContent() {
        return content;
    }

    public List<TextTreeNode> getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return (children != null);
    }
}
