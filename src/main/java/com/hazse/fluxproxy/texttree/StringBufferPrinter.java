package com.hazse.fluxproxy.texttree;

public class StringBufferPrinter implements IPrinter {
    private StringBuffer buffer;
    private String lineSeparator;

    public StringBufferPrinter(StringBuffer buffer) {
        this(buffer, System.lineSeparator());
    }

    public StringBufferPrinter(StringBuffer buffer, String lineSeparator) {
        this.buffer = buffer;
        this.lineSeparator = lineSeparator;
    }

    @Override
    public void print(String text) {
        buffer.append(text).append(lineSeparator);
    }
}
