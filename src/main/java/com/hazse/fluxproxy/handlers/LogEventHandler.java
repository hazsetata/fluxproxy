package com.hazse.fluxproxy.handlers;

import com.hazse.fluxproxy.event.AbstractEvent;
import com.hazse.fluxproxy.event.SyncEvent;
import com.hazse.fluxproxy.event.attribute.EventType;
import com.hazse.fluxproxy.event.attribute.GitCommit;
import com.hazse.fluxproxy.event.attribute.ResourceError;
import com.hazse.fluxproxy.event.attribute.ResourceId;
import com.hazse.fluxproxy.texttree.IPrinter;
import com.hazse.fluxproxy.texttree.TextTreeNode;
import com.hazse.fluxproxy.texttree.TextTreePrinter;
import com.hazse.fluxproxy.util.NamespaceComparator;

import java.util.ArrayList;
import java.util.List;

public class LogEventHandler implements IFluxEventHandler{
    private static final TextTreePrinter TREE_PRINTER = new TextTreePrinter();
    private static final NamespaceComparator NAMESPACE_COMPARATOR = new NamespaceComparator();

    private final IPrinter outputPrinter;

    public LogEventHandler(IPrinter outputPrinter) {
        this.outputPrinter = outputPrinter;
    }

    @Override
    public void handleFluxEvent(AbstractEvent event) {
        List<String> rootContent = new ArrayList<>();
        rootContent.add(event.getType().getPrintedName() + " event (id: " + event.getId() + ")");
        if (event.getMessage() != null) {
            rootContent.add("Message: " + event.getMessage());
        }
        rootContent.add("Started at : " + event.getStartedAt());
        rootContent.add("Finished at: " + event.getEndedAt());

        TextTreeNode root = new TextTreeNode(rootContent);

        List<ResourceId> affectedResources = event.getAffectedResources();
        if (affectedResources != null) {
            affectedResources.sort(
                    NAMESPACE_COMPARATOR
                            .thenComparing(ResourceId::getKind)
                            .thenComparing(ResourceId::getName)
            );
            TextTreeNode resources = root.addChild("Affected resources");
            for(ResourceId resourceId : affectedResources) {
                resources.addChild(formatResourceId(resourceId));
            }
        }
        else {
            root.addChild("No resources affected.");
        }

        if (event.getType().equals(EventType.Sync)) {
            printSyncDetails(((SyncEvent)event), root);
        }

        TREE_PRINTER.printTree(root, outputPrinter);
    }

    private void printSyncDetails(SyncEvent event, TextTreeNode root) {
        List<GitCommit> commits = event.getMetadata().getCommits();
        if ((commits != null) && (commits.size() > 0)) {
            TextTreeNode commitNode = root.addChild("Commits");
            for (GitCommit gitCommit : commits) {
                List<String> gitCommitNodeContent = new ArrayList<>();

                if (hasText(gitCommit.getMessage())) {
                    gitCommitNodeContent.add(gitCommit.getMessage());
                }
                gitCommitNodeContent.add("Revision: " + gitCommit.getRevision());

                commitNode.addChild(gitCommitNodeContent);
            }
        }

        List<ResourceError> errors = event.getMetadata().getErrors();
        if ((errors != null) && (errors.size() > 0)) {
            TextTreeNode errorNode = root.addChild("Errors");
            for (ResourceError error : errors) {
                errorNode.addChild(
                        error.getMessage(),
                        formatResourceId(error.getId()),
                        "Path: " + error.getPath()
                );
            }
        }

        root.addChild("Initial sync: " + event.getMetadata().isInitialSync());
    }

    @SuppressWarnings("UnusedReturnValue")
    private String formatResourceId(ResourceId resourceId) {
        if (resourceId.getNamespace() != null) {
            return "[" + resourceId.getNamespace() + "] " + resourceId.getKind() + "/" + resourceId.getName();
        }
        else {
            return resourceId.getKind() + "/" + resourceId.getName();
        }
    }

    private boolean hasText(String message) {
        return ((message != null) && (!message.trim().equals("")));
    }
}
