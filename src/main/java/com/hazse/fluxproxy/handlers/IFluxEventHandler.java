package com.hazse.fluxproxy.handlers;

import com.hazse.fluxproxy.event.AbstractEvent;

public interface IFluxEventHandler {
    void handleFluxEvent(AbstractEvent event);
}
