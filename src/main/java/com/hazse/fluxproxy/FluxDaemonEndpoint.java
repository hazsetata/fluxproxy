package com.hazse.fluxproxy;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/api/v1/fluxevents/v11/daemon")
public class FluxDaemonEndpoint {
    private static final Logger log = Logger.getLogger("fluxdaemon");

    @OnOpen
    public void onOpen(Session session) {
        log.debug("Connection with FluxCD established.");
    }

    @OnClose
    public void onClose(Session session) {
        log.debug("Connection with FluxCD closed.");
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("Connection with FluxCD closed with error.", error);
    }

    @OnMessage
    public void onTextMessage(Session session, String message) {
        log.info("Daemon text message: " + message);
    }

    @OnMessage
    public void onBinaryMessage(Session session, byte[] message) {
        log.info("Daemon binary message: " + Base64.encodeBase64String(message));
    }

    @OnMessage
    public void onPongMessage(Session session, PongMessage message) {
        log.info("Daemon pong message: " + message);
    }
}
